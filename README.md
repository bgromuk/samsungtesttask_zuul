Authorization and Resource servers based on Spring-Boot.

Resource server authorization check realized by Zuul Proxy.

Authorization token stored in cookies. Authorization requests sends by AngularJs.

spring-security-oauth-server running on port 8081
spring-security-oauth-ui-password running on port 8080

To start each server run mvn spring-boot:run (in each project path) embeded tomcat will run.

Also Authorization seerver has connection settings to mysql db stored in persistence.properties in resource path (please enter you credentials for db before start).

After that go to localhost:8080